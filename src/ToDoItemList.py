#!/usr/bin/env python

from PyQt4 import Qt, QtCore, QtGui
import sys

sys.path.append('./src')

from ToDoEntry import *
from ToDoItemAddWindow import *
from ToDoItemDetailWindow import *
from ToDoDefines import *


class ToDoItemList(QtGui.QListWidget):
    def __init__(self, QtNoteWin,  DbEngine, parent=None):
        self.homedir = QtNoteWin.config.get("main",  "homedir")
        QtGui.QListWidget.__init__(self, parent)
        self.setDragEnabled(True)
        self.setViewMode(QtGui.QListView.ListMode)
        self.setIconSize(QtCore.QSize(34, 34 ))
        self.setSpacing(2)
        self.setAcceptDrops(True)
        self.setDropIndicatorShown(True)
        self.setMovement(QtGui.QListView.Free)
        self.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        QtCore.QObject.connect(self, QtCore.SIGNAL("itemDoubleClicked(QListWidgetItem *)"), self.OpenItem)
        self.DbEngine = DbEngine
        self.QtNoteWin = QtNoteWin

    def OpenItem(self, item):
        oid = item.data(QtCore.Qt.UserRole).toInt()[0]
        res = self.DbEngine.CheckIfOpen(oid)
        if res == ITEM_CLOSED:
            self.DbEngine.SetItemOpen(oid)
            win = ItemDetailWindow(self.DbEngine, oid)
            dataDict = win.ItemDlgGui
            self.RefreshItem(item, dataDict)
        if res == ITEM_OPEN:
            QtGui.QMessageBox.warning(None, "Warning",
                    "Item already open",
                    QtGui.QMessageBox.Ok )
        self.QtNoteWin.RefreshSummaryList()
        self.QtNoteWin.UpdateItemList()
        
    def RefreshItem(self, item, data):
        item.setText(data.LE_CurHeadline.text())
        curSt = str(data.L_Status.text())
        item.setBackgroundColor(ST_COLOR[curSt])

    def addItem(self, recDict):
        listItem = QtGui.QListWidgetItem(self)
        pixmap = QtGui.QIcon(self.homedir+"/pixmaps/todo_list.png")
        listItem.setIcon(pixmap)
        listItem.setText(recDict["headline"])
        listItem.setData(QtCore.Qt.UserRole, QtCore.QVariant(recDict["oid"]))
        listItem.setData(QtCore.Qt.UserRole+1, QtCore.QVariant(TYPE_TODO))
        listItem.setBackgroundColor(ST_COLOR[recDict["status"]])
        listItem.setFlags(QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsDragEnabled)
        self.QtNoteWin.RefreshSummaryList()
        

    def dragEnterEvent(self, event):
        if event.mimeData().hasFormat("application/x-QtNote"):
            event.acceptProposedAction()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasFormat("application/x-QtNote"):
            event.setDropAction(QtCore.Qt.CopyAction)
            event.acceptProposedAction()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasFormat("application/x-QtNote"):
            pieceData = event.mimeData().data("application/x-QtNote")
            dataStream = QtCore.QDataStream(pieceData, QtCore.QIODevice.ReadOnly)
            oid = QtCore.QString()
            item_type = QtCore.QString()
            dataStream >> oid >> item_type
            if str(item_type) == TYPE_TODO:
                event.ignore()
                return
            data = {"oid": oid, "item": item_type}
            NewItem = ItemAddWindow(self.DbEngine, data)
            res = NewItem.ItemDlg.exec_()
            if res == QtGui.QDialogButtonBox.Save:
                Priority = NewItem.ItemDlgGui.CB_Priority.currentText()
                Body = NewItem.ItemDlgGui.TE_Body.toPlainText()
                dl = 0
                Headline = str(NewItem.ItemDlgGui.LE_Headline.text())
                timeout = 0
                if NewItem.ItemDlgGui.CB_Deadline.isChecked():
                    DeadLine = NewItem.ItemDlgGui.DTE_Deadline.dateTime().toString('yyyy MM dd hh mm')
                    t = DeadLine
                    dl = time.mktime((int(t[0:4]), int(t[5:7]),  int(t[8:10]),  int(t[11:13]),  int(t[14:16]),  0,  0,  0,  0))
                    tm = int(NewItem.ItemDlgGui.SP_timeout.value())
                    if tm > 0:
                        timeout = tm * 60
                dataDict = {
                    "body": str(Body),
                    "category": int(oid),
                    "priority": str(Priority),
                    "headline": Headline,
                    "status": 'Active',
                    "deadline" : dl,
                    "timeout" : timeout
                    }

                oid = self.DbEngine.InsertItem(dataDict)
                dataDict["oid"] = int(oid)
                event.setDropAction(QtCore.Qt.CopyAction)
                event.accept()
                self.addItem(dataDict)
            if res == QtGui.QDialogButtonBox.Close:
                event.ignore()
            self.QtNoteWin.UpdateItemList()
        else:
            event.ignore()

    def startDrag(self, supportedActions):
        item = self.currentItem()
        itemData = QtCore.QByteArray()
        dataStream = QtCore.QDataStream(itemData, QtCore.QIODevice.WriteOnly)
        oid_cat = item.data(QtCore.Qt.UserRole).toString()
        id_cat  = item.data(QtCore.Qt.UserRole+1).toString()
        id_type  = item.data(QtCore.Qt.UserRole+2).toString()
        dataStream << oid_cat << id_cat << id_type
        mimeData = QtCore.QMimeData()
        mimeData.setData("application/x-QtNote", itemData)

        drag = QtGui.QDrag(self)
        drag.setMimeData(mimeData)
        drag.setPixmap(QtGui.QPixmap(self.homedir+"/pixmaps/todo_list.png"))
        if drag.start(QtCore.Qt.MoveAction) == QtCore.Qt.MoveAction:
            self.takeItem(self.row(item))
