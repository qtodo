#!/usr/bin/env python

from PyQt4 import Qt, QtCore, QtGui
import sys


class ToDoEntry(QtGui.QFrame):   
    def __init__(self, DbEngine, parent=None):
        QtGui.QFrame.__init__(self, parent)
        self.setBackgroundRole(QtGui.QPalette.Light)
        self.L_Pr = QtGui.QLabel("Priority: ")
        self.L_St = QtGui.QLabel("Status: ")
        self.L_De = QtGui.QLabel("Deadline: ")
        self.L_PrImg = QtGui.QLabel(self)
        self.L_StImg = QtGui.QLabel(self)
        self.L_DeImg = QtGui.QLabel(self)
        self.L_PrImg.setPixmap(QtGui.QPixmap("pixmaps/house.png"))
        self.L_StImg.setPixmap(QtGui.QPixmap("pixmaps/house.png"))
        self.L_DeImg.setPixmap(QtGui.QPixmap("pixmaps/house.png"))
        self.TE_TodoBody = QtGui.QTextEdit(self)
        self.Layout = QtGui.QGridLayout(self)
        self.Layout.addWidget(self.L_Pr, 0,0)
        self.Layout.addWidget(self.L_PrImg, 0,1)
        self.Layout.addWidget(self.L_St, 0,2)
        self.Layout.addWidget(self.L_StImg, 0,3)
        self.Layout.addWidget(self.L_De, 0,4)
        self.Layout.addWidget(self.L_DeImg, 0,5)
        self.Layout.addWidget(self.TE_TodoBody, 1,0,1,5)
        self.setLayout(self.Layout)
        self.DbEngine = DbEngine
        
    def dropEvent_(self, event):
        if event.mimeData().hasFormat("application/x-QtNote"):
            pieceData = event.mimeData().data("application/x-QtNote")
            dataStream = QtCore.QDataStream(pieceData, QtCore.QIODevice.ReadOnly)
            
            oid_cat = QtCore.QString()
            id_cat = QtCore.QString()
            
            dataStream >> oid_cat >> id_cat
            event.setDropAction(QtCore.Qt.MoveAction)
            event.accept()
            # Delete the category from database
            self.DbEngine.DeleteList(oid_cat)


    def dropEvent_(self, event):
        if event.mimeData().hasFormat("application/x-QtNote"):
            itemData = event.mimeData().data("application/x-QtNote")
            dataStream = QtCore.QDataStream(itemData, QtCore.QIODevice.ReadOnly)
            text = QtCore.QString()
            dataStream >> text

                
    def dragEnterEvent(self, event):
        event.accept()                    
