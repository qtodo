#!/usr/bin/env python

from PyQt4 import Qt, QtCore, QtGui
import sys

sys.path.append('./src')

from gui_AddItemDlg import Ui_d_add_item

class ItemAddWindow(Ui_d_add_item):
    def __init__(self, DbEngine, data, parent=None):
        self.DbEngine = DbEngine
        self.data = data
        self.ItemDlg = QtGui.QDialog()
        self.ItemDlgGui = Ui_d_add_item()
        self.ItemDlgGui.setupUi(self.ItemDlg)
        self.FinalizeAddGui()


    def FinalizeAddGui(self):
        Pr = self.DbEngine.LoadPriorities()
        for p in Pr:
            self.ItemDlgGui.CB_Priority.addItem(p["id_pr"])
        Ct = self.DbEngine.GetCategories(self.data["oid"])
        self.ItemDlgGui.L_Category.setText(QtCore.QString(Ct[0]["id_cat"]))
        cur_time = QtCore.QDateTime().currentDateTime()
        self.ItemDlgGui.DTE_Deadline.setDateTime(cur_time)
        QtCore.QObject.connect(self.ItemDlgGui.B_Save,QtCore.SIGNAL("clicked()"),self.Save)
        QtCore.QObject.connect(self.ItemDlgGui.B_Close,QtCore.SIGNAL("clicked()"),self.Close)

    def Save(self):
        if str(self.ItemDlgGui.LE_Headline.text()) == '':
            res = QtGui.QMessageBox.warning(None, "Warning",
            "The Headline is required",
            QtGui.QMessageBox.Ok )        
            return
        self.ItemDlg.done(QtGui.QDialogButtonBox.Save)
        
    def Close(self):
        self.ItemDlg.done(QtGui.QDialogButtonBox.Close)
        
