#!/usr/bin/env python

from PyQt4 import QtGui

VERSION = "0.2.x"

ST_ACTIVE = 'Active'
ST_CLOSED = 'Closed'
ST_OVERDUE = 'Overdue'

TYPE_LIST = 'List'
TYPE_TODO = 'Item'

ST_COLOR = {
    ST_CLOSED : QtGui.QColor(192,192,192,64),
    ST_ACTIVE : QtGui.QColor(0,192,0,64),
    ST_OVERDUE : QtGui.QColor(192,0,0,64)
}

ALL_RECORD = 'All'


#Errors
DUPLICATED_CATEGORY = -101

ITEM_OPEN = 102
ITEM_CLOSED = 103

# Debug
DEBUG = 1
