#!/usr/bin/env python

from PyQt4 import Qt, QtCore, QtGui
import sys,  time

sys.path.append('./src')

from gui_ItemDetailDlg import Ui_D_item_detail
from ToDoDefines import *

class ItemDetailWindow(Ui_D_item_detail):
    def __init__(self, DbEngine, oid, parent=None):
        self.DbEngine = DbEngine
        self.Modified = False
        self.record = self.DbEngine.GetItems(None, oid)
        self.record = self.record[0]
        self.curRecOid = oid
        self.ItemDlg = QtGui.QDialog()
        self.ItemDlgGui = Ui_D_item_detail()
        self.ItemDlgGui.setupUi(self.ItemDlg)
        self.FinalizeDetailGui()

        self.ItemDlg.show()
        self.ItemDlg.exec_()


    def FinalizeDetailGui(self):
        cats = self.DbEngine.GetCategories()
        c = 0
        d = 0
        for cat in cats:
            self.ItemDlgGui.CB_Categories.addItem(cat["id_cat"])
            if self.record["category"] == cat["id_cat"]:
                d = c
            c += 1
        self.ItemDlgGui.CB_Categories.setCurrentIndex(d)

        Pr = self.DbEngine.LoadPriorities()
        c = 0
        d = 0
        for p in Pr:
            self.ItemDlgGui.CB_Priorities.addItem(p["id_pr"])
            if p["id_pr"] == self.record["priority"]:
                d = c
            c += 1
        self.ItemDlgGui.CB_Priorities.setCurrentIndex(d)

        if self.record["status"] == ST_OVERDUE:
            f = self.ItemDlgGui.L_Status.palette()
            f.setColor(QtGui.QPalette.Foreground,QtGui.QColor(255, 0, 0))
            self.ItemDlgGui.L_Status.setPalette(f)
        self.ItemDlgGui.L_Status.setText(str(self.record["status"]))

        self.ItemDlgGui.LE_CurHeadline.setText(self.record["headline"])
        self.ItemDlgGui.TB_CurBody.setText(self.record["body"])
        if self.record["deadline"] == 0:
            cur_time = QtCore.QDateTime().currentDateTime()
        else:
            cur_time = QtCore.QDateTime().fromString(self.record["deadline"], "dd.MM.yyyy hh:mm")
            self.ItemDlgGui.CB_Deadline.setCheckState(QtCore.Qt.Checked)
        self.ItemDlgGui.DTE_CurDeadline.setDateTime(cur_time)
        self.ItemDlgGui.SP_timeout.setValue(self.record["timeout"])

        # Connecting the signal to the custom slot
        QtCore.QObject.connect(self.ItemDlgGui.B_ItemDetClose, QtCore.SIGNAL("clicked()"), self.CloseDetail)
        QtCore.QObject.connect(self.ItemDlgGui.B_CloseItem, QtCore.SIGNAL("clicked()"), self.SetItemAsDone)
        QtCore.QObject.connect(self.ItemDlgGui.B_ReopenItem, QtCore.SIGNAL("clicked()"), self.SetItemAsOpen)

        QtCore.QObject.connect(self.ItemDlgGui.CB_Categories, QtCore.SIGNAL("currentIndexChanged(int)"), self.UpdateCurValues)
        QtCore.QObject.connect(self.ItemDlgGui.CB_Priorities, QtCore.SIGNAL("currentIndexChanged(int)"), self.UpdateCurValues)
        QtCore.QObject.connect(self.ItemDlgGui.DTE_CurDeadline, QtCore.SIGNAL("dateTimeChanged(QDateTime)"), self.UpdateCurValues)
        QtCore.QObject.connect(self.ItemDlgGui.LE_CurHeadline, QtCore.SIGNAL("textChanged(QString)"), self.UpdateCurValues)
        QtCore.QObject.connect(self.ItemDlgGui.TB_CurBody, QtCore.SIGNAL("textChanged()"), self.UpdateCurValues)
        QtCore.QObject.connect(self.ItemDlgGui.CB_Deadline, QtCore.SIGNAL("stateChanged(int)"), self.UpdateCurValues)


    def SetItemAsDone(self):
        self.record["status"] = ST_CLOSED
        self.ItemDlgGui.L_Status.setText(ST_CLOSED)
        f = self.ItemDlgGui.L_Status.palette()
        f.setColor(QtGui.QPalette.Foreground,QtGui.QColor(0, 0, 0))
        self.Modified = True
        
    def SetItemAsOpen(self):
        self.record["status"] = ST_ACTIVE
        self.ItemDlgGui.L_Status.setText(ST_ACTIVE)
        f = self.ItemDlgGui.L_Status.palette()
        f.setColor(QtGui.QPalette.Foreground,QtGui.QColor(0, 0, 0))
        self.Modified = True
        
    def UpdateCurValues(self, index=None):
        self.record["headline"] = str(self.ItemDlgGui.LE_CurHeadline.text())
        self.record["body"] = str(self.ItemDlgGui.TB_CurBody.toPlainText())
        self.record["category"] = str(self.ItemDlgGui.CB_Categories.currentText())
        if self.ItemDlgGui.CB_Deadline.checkState():
            t = self.ItemDlgGui.DTE_CurDeadline.dateTime().toString('yyyy MM dd hh mm')
            dl = time.mktime((int(t[0:4]), int(t[5:7]),  int(t[8:10]),  int(t[11:13]),  int(t[14:16]),  0,  0,  0,  0))
            self.record["deadline"] = dl
        else:
            self.record["deadline"] = 0
        if self.record["deadline"] < time.time() - 60 and self.record["deadline"] > 0:
            self.ItemDlgGui.L_Status.setText(ST_OVERDUE)
            f = self.ItemDlgGui.L_Status.palette()
            f.setColor(QtGui.QPalette.Foreground,QtGui.QColor(255, 0, 0))
        else:
            self.ItemDlgGui.L_Status.setText(ST_ACTIVE)
            f = self.ItemDlgGui.L_Status.palette()
            f.setColor(QtGui.QPalette.Foreground,QtGui.QColor(0, 0, 0))

        self.ItemDlgGui.L_Status.setPalette(f)
        self.record["priority"] = str(self.ItemDlgGui.CB_Priorities.currentText())
        self.record["status"] = str(self.ItemDlgGui.L_Status.text())

        self.Modified = True

    def CloseDetail(self):
        if self.Modified == True:
            res = QtGui.QMessageBox.question(None, "Save changes ?",
            "There are unsaved changes, save before closing ?",
            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No | QtGui.QMessageBox.Default | QtGui.QMessageBox.Escape)
            if res == QtGui.QMessageBox.Yes:
                self.record["oid"] = self.curRecOid
                res = self.DbEngine.UpdateItem(self.record)
                self.Modified = False
                self.ItemDlg.accept()
                self.DbEngine.SetItemClosed(self.curRecOid)
            if res == QtGui.QMessageBox.No:
                self.Modified = False
        if self.Modified == False:
            self.DbEngine.SetItemClosed(self.curRecOid)
            self.ItemDlg.accept()



