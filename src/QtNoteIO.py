#!/usr/bin/env python

import os, re, string, time

from pysqlite2 import dbapi2 as sqlite
from ToDoDefines import *

class QtNoteIO:
    def __init__(self,  config):
        dbFile = config.get("database",  "filepath")
        self.DBcon = sqlite.connect(dbFile)

    def GetCategories(self, cat=None):
        data = []
        cur = self.DBcon.cursor()
        query = "select oid, id_category from categories"
        if cat != None:
            try:
                query += " where oid = %d"%int(cat)
            except:
                query += " where id_category = '%s'"%str(cat)
        cur.execute(query)
        results = cur.fetchall()
        for res in results:
            data.append({"oid": res[0], "id_cat": res[1].encode('latin-1')})
        return data

    def CheckForDeadlines(self):
        curtime = time.time()
        query = "select oid, deadline from items where deadline > 0 "
        cur = self.DBcon.cursor()
        cur.execute(query)
        results = cur.fetchall()
        for res in results:
            t = res[1]
            if t > 0:
                if t < curtime:
                    query = "update items set status = (select oid from status where desc = '%s') where oid = %d"%(ST_OVERDUE,  res[0])
                    cur.execute(query)
        self.DBcon.commit()


    def InsertCategory(self, dataDict):
        cur = self.DBcon.cursor()
        query = "select oid from categories where id_category ='%s'"%dataDict["id_category"]
        cur.execute(query)
        res = cur.fetchall()
        if len(res) > 0:
            return DUPLICATED_CATEGORY
        query = "insert into categories (id_category, ds_category) values ('%s', '%s')"%(dataDict["id_category"], dataDict["ds_category"])
        cur.execute(query)
        self.DBcon.commit()
        query = "select oid from categories where id_category='%s'"%dataDict["id_category"]
        cur.execute(query)
        results = cur.fetchall()[0]
        oid = results[0]
        return oid


    def DeleteList(self, oid):
        cur = self.DBcon.cursor()
        query = "select count(*) from items where category = %d"%int(oid)
        cur.execute(query)
        results = cur.fetchall()[0]
        it = results[0]
        if int(it) > 0:
            return -1
        query = "delete from categories where oid = %d"%int(oid)
        cur.execute(query)
        self.DBcon.commit()

    def DeleteItem(self, oid):
        cur = self.DBcon.cursor()
        query = "delete from items where oid = %d"%int(oid)
        cur.execute(query)
        self.DBcon.commit()


    def CountItems(self,  status):
        cur = self.DBcon.cursor()
        cur.execute("select count(*) from items where status = (select oid from status where desc = '%s')"%status)
        results = cur.fetchall()[0]
        cA = results[0]
        return cA


    def LoadPriorities(self):
        data = []
        cur = self.DBcon.cursor()
        cur.execute("select * from priority")
        results = cur.fetchall()
        for res in results:
            data.append({"oid": res[0], "id_pr": res[1].encode('latin-1')})
        return data

    def GetPriority(self, inputData):
        cur = self.DBcon.cursor()
        query = "select oid from priority where priority = '%s'"%inputData
        cur.execute(query)
        results = cur.fetchall()[0]
        oidPr = results[0]
        return oidPr

    def GetCategory(self, inputData):
        cur = self.DBcon.cursor()
        query = "select oid from categories where id_category = '%s'"%inputData
        cur.execute(query)
        results = cur.fetchall()[0]
        oidPr = results[0]
        return oidPr

    def LoadStatus(self):
        data = []
        cur = self.DBcon.cursor()
        cur.execute("select oid, desc from status")
        results = cur.fetchall()
        for res in results:
            data.append({"oid": res[0],  "desc": res[1].encode('latin-1')})
        return data

    def GetStatus(self, status):
        cur = self.DBcon.cursor()
        cur.execute("select oid from status where desc ='%s'"%status)
        results = cur.fetchall()[0]
        oidSt = results[0]
        return oidSt


    def InsertItem(self, dataDict):
        cur = self.DBcon.cursor()
        oidPr_ = self.GetPriority(dataDict["priority"])
        oidPr = int(oidPr_)

        query = """insert into items (headline, body, category, deadline, priority, status, opened, timeout)
        values ('%s', '%s', %d, %d ,%d, 1,0,%d)"""%(dataDict["headline"], dataDict["body"], dataDict["category"], dataDict["deadline"], oidPr, dataDict["timeout"])

        cur.execute(query)
        self.DBcon.commit()
        cur.execute("select max(oid) from items")
        results = cur.fetchall()[0]
        oid = results[0]
        return oid

    
    def GetNextAlarm(self):
        cur_time = time.time()
        return 1
        
    def UpdateItem(self, dataDict):
        cur = self.DBcon.cursor()
        oidPr_ = self.GetPriority(dataDict["priority"])
        oidPr = int(oidPr_)
        oidCat = self.GetCategory(dataDict["category"])
        oidCat = int(oidCat)
        oidSt = self.GetStatus(dataDict["status"])
        oidSt = int(oidSt)
        query = """update items
            set headline = "%s",
            body = "%s",
            category = %d,
            deadline = %d,
            priority = %d,
            status = %d
            where oid = %d
        """%(dataDict["headline"].strip(), dataDict["body"].strip(), oidCat, dataDict["deadline"], oidPr, oidSt, dataDict["oid"])
        cur.execute(query)
        self.DBcon.commit()
        return 0

    def GetItems(self, DictFilters, oid=None):
        data = []
        cur = self.DBcon.cursor()
        query = """
            select i.oid, i.headline, c.id_category, p.priority, i.body, i.deadline, s.desc, i.timeout from items i
            inner join categories c
            on c.oid = i.category
            inner join priority p
            on p.oid = i.priority
            inner join status s
            on i.status = s.oid
            where 0=0
        """
        if DictFilters != None:
            if DictFilters["Oid"] != None:
                query += " and i.oid = %d"%(int(DictFilters["Oid"]))
            if DictFilters["OidCategory"]!= None:
                query += " and c.oid = %d"%(int(DictFilters["OidCategory"]))
            if DictFilters["OidPriority"] != None:
                query += " and p.oid = %d"%(int(DictFilters["OidPriority"]))
            if DictFilters["OidStatus"] != None:
                query += " and s.oid = %d"%(int(DictFilters["OidStatus"]))

        if oid != None:
            query += " and i.oid = %d"%(int(oid))
            
        query_deadline = query +" and i.deadline > 0 order by i.deadline asc"
        query_ND = query +" and i.deadline = 0"
        cur.execute(query_deadline)
        results = cur.fetchall()
        for res in results:
            dl = 0
            timeout = 0
            if res[5] != 0:  #dd.MM.yyyy hh:mm
                dl = time.strftime('%d.%m.%Y %H:%M',time.localtime(res[5])).encode('latin-1')
            if res[7] != 0:  #dd.MM.yyyy hh:mm
                timeout = res[7] / 60
            data.append({"oid": str(res[0]),
                         "headline": res[1].encode('latin-1'),
                         "category": res[2].encode('latin-1'),
                         "priority": res[3].encode('latin-1'),
                         "body": res[4].encode('latin-1'),
                         "deadline": dl,
                         "status": res[6].encode('latin-1'),
                         "timeout" : timeout
            })
        
        cur.execute(query_ND)
        resultsNoDead = cur.fetchall()
        for res in resultsNoDead:
            data.append({"oid": str(res[0]),
                         "headline": res[1].encode('latin-1'),
                         "category": res[2].encode('latin-1'),
                         "priority": res[3].encode('latin-1'),
                         "body": res[4].encode('latin-1'),
                         "deadline": 0,
                         "status": res[6].encode('latin-1'),
                         "timeout" : 1
            })
            

        return data

    def CheckIfOpen(self,  oid):
        query = "select opened from items where oid = %d"%oid
        cur = self.DBcon.cursor()
        cur.execute(query)
        res = cur.fetchall()[0]
        if int(res[0]) == 0:
            return ITEM_CLOSED
        if int(res[0]) == 1:
            return ITEM_OPEN

    def SetItemOpen(self,  oid):
        cur = self.DBcon.cursor()
        cur.execute("update items set opened = 1 where oid = %d"%oid)
        self.DBcon.commit()

    def SetItemClosed(self,  oid):
        cur = self.DBcon.cursor()
        cur.execute("update items set opened = 0 where oid = %d"%oid)
        self.DBcon.commit()

