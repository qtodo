#!/usr/bin/env python

from PyQt4 import Qt, QtCore, QtGui
import sys

sys.path.append('./src')

from ToDoDefines import *


class TodoLists(QtGui.QListWidget):
    def __init__(self, QtNoteWin, parent=None):
        QtGui.QListWidget.__init__(self, parent)
        self.homedir = QtNoteWin.config.get("main",  "homedir")
        self.setDragEnabled(True)
        self.setViewMode(QtGui.QListView.ListMode)
        self.setIconSize(QtCore.QSize(34, 34 ))
        self.setSpacing(5)
        self.setAcceptDrops(True)
        self.setDropIndicatorShown(True)
        self.setMovement(QtGui.QListView.Free)
        self.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        
    def addList(self, oid_cat, id_cat):
        listItem = QtGui.QListWidgetItem(self)
        pixmap = QtGui.QIcon(self.homedir+"/pixmaps/todo_list.png")
        listItem.setIcon(pixmap)
        listItem.setText(id_cat)
        listItem.setData(QtCore.Qt.UserRole, QtCore.QVariant(oid_cat))
        listItem.setData(QtCore.Qt.UserRole+1, QtCore.QVariant(TYPE_LIST))
        listItem.setFlags(QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsDragEnabled)
        

    def dragEnterEvent(self, event):
        if event.mimeData().hasFormat("application/x-QtNote"):
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasFormat("application/x-QtNote"):
            event.setDropAction(QtCore.Qt.MoveAction)
            event.accept()
        else:
            event.ignore()
            
    def dropEvent_(self, event):
        if event.mimeData().hasFormat("application/x-QtNote"):
            pieceData = event.mimeData().data("application/x-QtNote")
            dataStream = QtCore.QDataStream(pieceData, QtCore.QIODevice.ReadOnly)
            pixmap = QtGui.QPixmap()
            location = QtCore.QPoint()
            dataStream >> pixmap >> location

            self.addList(pixmap, location)

            event.setDropAction(QtCore.Qt.MoveAction)
            event.accept()
        else:
            event.ignore()

    def startDrag(self, supportedActions):
        item = self.currentItem()
        itemData = QtCore.QByteArray()
        dataStream = QtCore.QDataStream(itemData, QtCore.QIODevice.WriteOnly)
        oid = item.data(QtCore.Qt.UserRole).toString()
        item_type  = item.data(QtCore.Qt.UserRole+1).toString()
        dataStream << oid << item_type

        mimeData = QtCore.QMimeData()
        mimeData.setData("application/x-QtNote", itemData)
        drag = QtGui.QDrag(self)
        drag.setMimeData(mimeData)
        drag.setPixmap(QtGui.QPixmap(self.homedir+"/pixmaps/todo_list.png"))

        if drag.start(QtCore.Qt.MoveAction) == QtCore.Qt.MoveAction:
            self.takeItem(self.row(item))
