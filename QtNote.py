#!/usr/bin/env python



from PyQt4 import Qt, QtCore, QtGui
import sys,  time,  os,  ConfigParser

homeEnv = os.getenv('HOME')
config = ConfigParser.ConfigParser()
try:
    config.readfp(open('QtNote.cfg'))
except:
    config.readfp(open(homeEnv+'/.QtNote.cfg'))
    
homedir = config.get("main",  "homedir")

sys.path.append(homedir+'/src')

from QtNoteIO import *
from gui_MainWin import *
from gui_AddCatDlg import Ui_D_Add
from ToDoCategoryList import *
from ToDoItemList import *


class Trash(QtGui.QLabel):
    def __init__(self, QtNoteWin,  DbEngine, parent=None):
        QtGui.QLabel.__init__(self, parent)
        self.setAcceptDrops(True)
        self.setPixmap(QtGui.QPixmap(homedir+"/pixmaps/eraser.png"))
        self.setSizePolicy(QtGui.QSizePolicy.Minimum,QtGui.QSizePolicy.Minimum)
        self.setMargin(2)
        self.adjustSize()
        self.DbEngine = DbEngine
        self.QtNoteWin = QtNoteWin

    def dropEvent(self, event):
        if event.mimeData().hasFormat("application/x-QtNote"):
            pieceData = event.mimeData().data("application/x-QtNote")
            dataStream = QtCore.QDataStream(pieceData, QtCore.QIODevice.ReadOnly)
            oid = QtCore.QString()
            item_type = QtCore.QString()
            dataStream >> oid >> item_type
            event.setDropAction(QtCore.Qt.MoveAction)
            if item_type == TYPE_LIST:
                # Delete the category from database
                res = self.DbEngine.DeleteList(oid)
                if res == -1:
                    res = QtGui.QMessageBox.critical(None, "Error",
                    "There are some items still assigned to this category",
                    QtGui.QMessageBox.Ok )
                    self.setFrameShape(QtGui.QFrame.NoFrame)
                    self.setFrameShadow(QtGui.QFrame.Plain)
                    event.ignore()
                else:
                    event.accept()
            if item_type == TYPE_TODO:
                # Delete the category from database
                res = self.DbEngine.DeleteItem(oid)
                event.accept()
        else:
            event.ignore()
        self.setFrameShape(QtGui.QFrame.NoFrame)
        self.setFrameShadow(QtGui.QFrame.Plain)
        self.QtNoteWin.RefreshSummaryList()

    def dragEnterEvent(self, event):
        self.setFrameShape(QtGui.QFrame.Panel)
        self.setFrameShadow(QtGui.QFrame.Raised)
        event.accept()

    def dragLeaveEvent(self, event):
        self.setFrameShape(QtGui.QFrame.NoFrame)
        self.setFrameShadow(QtGui.QFrame.Plain)

class AddList(QtGui.QLabel):
    def __init__(self, DbEngine, parent=None):
        QtGui.QLabel.__init__(self, parent)
        self.setAcceptDrops(True)
        self.setPixmap(QtGui.QPixmap(homedir+"/pixmaps/eraser.png"))
        self.DbEngine = DbEngine

    def dropEvent(self, event):
        if event.mimeData().hasFormat("application/x-QtNote"):
            pieceData = event.mimeData().data("application/x-QtNote")
            dataStream = QtCore.QDataStream(pieceData, QtCore.QIODevice.ReadOnly)

            oid_cat = QtCore.QString()
            id_cat = QtCore.QString()

            dataStream >> oid_cat >> id_cat
            event.setDropAction(QtCore.Qt.MoveAction)
            event.accept()
            # Delete the category from database
            self.DbEngine.DeleteList(oid_cat)

    def dragEnterEvent(self, event):
        event.accept()

class QtNote(Ui_MainWindow):
    def __init__(self,  parent=None):
        self.config = config
        self.IOEngine = QtNoteIO(self.config)
        self.Filters = {
            "Oid" : None,
            "OidCategory" : None,
            "OidPriority" : None,
            "OidStatus" : None
        }
        
        
    def Finalize(self):
        # set the trash zone
        ui.trash = Trash(ui,  self.IOEngine)
        ui.actions_box.layout().addWidget(ui.trash)

        # insert a stretch to keep aligned the controls
        ui.actions_box.layout().insertStretch(-1, 20)

        # set the todo category area
        ui.todo_lists = TodoLists(ui)
        ui.lists_box.layout().addWidget(ui.todo_lists, 0, 0)
        ui.todo_lists.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Preferred)
        ui.todo_items = ToDoItemList(ui,   self.IOEngine)
        ui.items_box.layout().addWidget(ui.todo_items, 1,  0)


        # check for new overdue items
        self.IOEngine.CheckForDeadlines()
        self.RefreshSummaryList()

        cats = self.IOEngine.GetCategories()
        ui.CB_CategoryFilter.addItem('All')
        for c in cats:
            ui.CB_CategoryFilter.addItem(c["id_cat"])

        stats = self.IOEngine.LoadStatus()
        ui.CB_StatusFilter.addItem('All')
        for s in stats:
            ui.CB_StatusFilter.addItem(s["desc"])

        prior = self.IOEngine.LoadPriorities()
        ui.CB_PriorityFilter.addItem('All')
        for p in prior:
            ui.CB_PriorityFilter.addItem(p["id_pr"])

        QtCore.QObject.connect(ui.action_Quit, QtCore.SIGNAL("triggered()"), self.QuitApp)
        QtCore.QObject.connect(ui.action_About, QtCore.SIGNAL("triggered()"), self.About)
        QtCore.QObject.connect(ui.action_Documentation, QtCore.SIGNAL("triggered()"), self.Documentation)
        QtCore.QObject.connect(ui.action_Category, QtCore.SIGNAL("triggered()"), self.CategoryConf)

        QtCore.QObject.connect(ui.CB_CategoryFilter, QtCore.SIGNAL("currentIndexChanged(QString)"), self.UpdateCategoryFilter)
        QtCore.QObject.connect(ui.CB_PriorityFilter, QtCore.SIGNAL("currentIndexChanged(QString)"), self.UpdatePriorityFilter)
        QtCore.QObject.connect(ui.CB_StatusFilter, QtCore.SIGNAL("currentIndexChanged(QString)"), self.UpdateStatusFilter)


    def LoadData(self):
        categories = self.IOEngine.GetCategories()
        for rec in categories:
            ui.todo_lists.addList(rec["oid"], rec["id_cat"])
        self.RefreshItemList()


    def RefreshItemList(self):
        items = self.IOEngine.GetItems(self.Filters)
        for rec in items:
            ui.todo_items.addItem(rec)

    def RefreshSummaryList(self):
        cA = self.IOEngine.CountItems(ST_ACTIVE)
        cC = self.IOEngine.CountItems(ST_CLOSED)
        cO = self.IOEngine.CountItems(ST_OVERDUE)
        ui.L_ActiveItem.setText(str(cA))
        ui.L_CloseItem.setText(str(cC))
        if cO > 0:
            f = ui.L_OverdueItem.palette()
            f.setColor(QtGui.QPalette.Foreground,QtGui.QColor(255, 0, 0))
            ui.L_OverdueItem.setPalette(f)
        ui.L_OverdueItem.setText(str(cO))
        
    def QuitApp(self):
        global PidFilePath
        try:
            os.remove(PidFilePath)
        except:
            pass
        systray.hide()
        app.quit()
    
    def UpdateCategoryFilter(self, text):
        if str(text) == ALL_RECORD:
            self.Filters["OidCategory"] = None
        else:
            oidcat = self.IOEngine.GetCategory(str(text))
            self.Filters["OidCategory"] = oidcat
        self.UpdateItemList()

    def UpdatePriorityFilter(self, text):
        if str(text) == ALL_RECORD:
            self.Filters["OidPriority"] = None
        else:
            oidpr = self.IOEngine.GetPriority(str(text))
            self.Filters["OidPriority"] = oidpr
        self.UpdateItemList()

    def UpdateStatusFilter(self, text):
        if str(text) == ALL_RECORD:
            self.Filters["OidStatus"] = None
        else:
            oidsta = self.IOEngine.GetStatus(str(text))
            self.Filters["OidStatus"] = oidsta
        self.UpdateItemList()

    def UpdateItemList(self):
        ui.todo_items.clear()
        self.RefreshItemList()


    def CategoryConf(self):
        AddCatDlg = QtGui.QDialog()
        AddCat = Ui_D_Add()
        AddCat.setupUi(AddCatDlg)
        if AddCatDlg.exec_():
            cat = AddCat.LE_cat_name.text()
            if str(cat) == '':
                return
            desc = AddCat.LE_cat_desc.text()
            Data = {"id_category" : str(cat),
                    "ds_category" : str(desc) }
            oid_cat = self.IOEngine.InsertCategory(Data)
            if oid_cat == DUPLICATED_CATEGORY:
                res = QtGui.QMessageBox.critical(None, "Error",
                    "The category is already present",
                    QtGui.QMessageBox.Ok )
                return
            it = QtGui.QListWidgetItem(QtGui.QIcon(homedir+"/pixmaps/todo_list.png"),cat)
            it.setTextAlignment(QtCore.Qt.AlignVCenter)
            ui.todo_lists.insertItem(0,it)
            it.setData(QtCore.Qt.UserRole, QtCore.QVariant(oid_cat))
            it.setData(QtCore.Qt.UserRole+1, QtCore.QVariant(TYPE_LIST))


    def DeleteCategory(self):
        pass

    def EditCategory(self):
        pass

    def About(self):
        QtGui.QMessageBox.about(None, "About QNotes",  "QNotes Ver %s.\nA simple todo manager"%VERSION)


    def Documentation(self):
        pass

class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
    
    def closeEvent(self,  ev):
        self.hide()
        systray.showMessage('QtNotes',  'Application minimized')
        ev.ignore()

    def ShowWindow(self):
        self.show()

    # implement also on minimize window

def CheckForPid():
    try:
        res = os.stat(PidFilePath)
        return -1
    except:
        PF = open(PidFilePath,  'w')
        PF.close()
        return 0

def ShowAlarm():
    print "Alarm work"


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)

    PidFilePath = os.environ["HOME"]+"/.QtNotes"
    if os.sys.platform == "win32":
        PidFilePath = os.environ["TEMP"]+"/.QtNotes"
    res = CheckForPid()
    res = 0
    if res == -1:
        QtGui.QMessageBox.critical(None,
            "QtNotes - Error",
            "The program is alred running",
            QtGui.QMessageBox.StandardButtons(\
                QtGui.QMessageBox.Ok),
            QtGui.QMessageBox.Ok)
        sys.exit()

    #window = QtGui.QMainWindow()
    window = MainWindow()
    ui = QtNote()
    ui.setupUi(window)
    window.setWindowTitle(str(window.windowTitle()) + " " +VERSION)

    #connect the menu items
    ui.Finalize()
    #Load the data present
    ui.LoadData()
    window.show()

    app.connect(app, QtCore.SIGNAL("lastWindowClosed()"),ui.QuitApp)

    icon=QtGui.QIcon(homedir+"/pixmaps/todo_list.png")
    systray=QtGui.QSystemTrayIcon(icon)
    menu = QtGui.QMenu()
    showAction = menu.addAction('Show')
    sep1 = menu.addSeparator()
    quitAction = menu.addAction('Quit')
    QtCore.QObject.connect(quitAction,  QtCore.SIGNAL("triggered()"), ui.QuitApp)    
    QtCore.QObject.connect(showAction,  QtCore.SIGNAL("triggered()"), window.ShowWindow)    
    systray.setContextMenu(menu)
    systray.show()

    # Imposto il timer per il primo evento che scade.
    value = ui.IOEngine.GetNextAlarm()
    print value
    timer = QtCore.QTimer()
    QtCore.QObject.connect(timer,  QtCore.SIGNAL("timeout()"), ShowAlarm)   
    timer.setSingleShot(True)
    timer.start(1000)
    
    sys.exit(app.exec_())
